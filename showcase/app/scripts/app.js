'use strict';

/**
 * @ngdoc overview
 * @name showcaseApp
 * @description
 * # showcaseApp
 *
 * Main module of the application.
 */
var app =angular
  .module('showcaseApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'mainCtrl'
      })
      .when('/recherche-utilisateur', {
        templateUrl: 'views/recherche-utilisateur.html',
        controller: 'RechercheCtrl',
        controllerAs: 'searchCtrl'
      })
      .when('/ajouter-utilisateur', {
        templateUrl: 'views/ajouter-utilisateur.html',
        controller: 'AddUserCtrl',
        controllerAs: 'addUserCtrl'
      })
      .when('/recherche-projet', {
        templateUrl: 'views/recherche-projet.html',
        controller: 'RechercheProjetCtrl',
        controllerAs: 'searchProjectCtrl'
      })
      .when('/ajouter-projet', {
        templateUrl: 'views/ajouter-projet.html',
        controller: 'AddProjectCtrl',
        controllerAs: 'addProjectCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

app.directive("navTab", function() {
  return {
    restrict: "E",
    templateUrl: "views/nav-tab.html",
    controller: 'TabCtrlCookie',
    controllerAs: "tabCookie"
  };
});
