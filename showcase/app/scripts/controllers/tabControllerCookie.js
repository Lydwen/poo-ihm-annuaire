/**
 * Created by webdev on 4/14/15.
 */

var app=angular.module('showcaseApp')
  .controller('TabCtrlCookie', ['$cookieStore',function($cookieStore) {
  this.tab=$cookieStore.get('rank');
  if(this.tab==null){
    this.tab=0;
  }
  this.isSet = function(checkTab) {
    return this.tab === checkTab;
  };

  this.setTab = function(activeTab) {
    this.tab = activeTab;
    $cookieStore.remove('rank');
    $cookieStore.put('rank',activeTab);
  };
}]);

