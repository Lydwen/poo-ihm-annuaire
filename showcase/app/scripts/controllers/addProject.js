app.controller('AddProjectCtrl',['AddProject', function(AddProject){
  this.title;
  this.description;
  this.year;
  this.addToBDD=false;
  var self=this;
  this.addProject = function(){
    AddProject.add(self.title,self.description,self.year,function(){
        self.addToBDD=true;
      },
      function(){
        self.erreur="Remplissez correctement le formulaire s'il-vous-plaît.";
        self.addToBDD=false;
      }
    );
  };

  this.clearForm=function(){
    document.getElementById("addForm").reset();
    self.addToBDD=false;
  };
}]);


app.service('AddProject',['$http', function AddProject($http){
  this.add=function(title,description,year,successFunc,failFunc){
    this.newProject={};
    this.newProject.title= title;
    this.newProject.description= description;
    this.newProject.year= year;
    $http.post('http://poo-ihm-2015-rest.herokuapp.com/api/Projects/', this.newProject)
      .success(function(data) {
        if(data.status==="success"){
          successFunc();
        }else{
          failFunc();
        }
      });
  }
}]);

