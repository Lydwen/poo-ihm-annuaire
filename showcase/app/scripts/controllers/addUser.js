app.controller('AddUserCtrl',['AddUser', function(AddUser){
  this.name;
  this.surname;
  this.mail;
  this.site;
  this.addToBDD=false;
  var self=this;
  this.addUser = function(){
    AddUser.add(self.name,self.surname,self.mail,self.site,function(){
        self.addToBDD=true;
      },
      function(){
        self.erreur="Remplissez correctement le formulaire s'il-vous-plaît.";
        self.addToBDD=false;
      }
    );
  };

  this.clearForm=function(){
    document.getElementById("addForm").reset();
    self.addToBDD=false;

  };
}]);


app.service('AddUser',['$http', function AddUser($http){
  this.add=function(name,surname,mail,site,successFunc,failFunc){
    this.newUser={};
    this.newUser.name= name;
    this.newUser.surname= surname;
    this.newUser.email= mail;
    this.newUser.website= site;
    $http.post('http://poo-ihm-2015-rest.herokuapp.com/api/Users/', this.newUser)
      .success(function(data) {
        if(data.status==="success"){
          successFunc();
        }else{
          failFunc();
        }
    });
  }
}]);

