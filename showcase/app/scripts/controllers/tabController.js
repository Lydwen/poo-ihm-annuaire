/**
 * Created by webdev on 4/14/15.
 */

var app=angular.module('showcaseApp')
  .controller('TabCtrl',function() {
    this.tab=0;

    this.isSet = function(checkTab) {
      return this.tab === checkTab;
    };

    this.setTab = function(activeTab) {
      this.tab = activeTab;
    };
  });

