/**
 * @ngdoc function
 * @name showcaseApp.controller:RechercheProject
 * @description
 * # RechercheProjectCtrl
 * Controller of the search project in the showcaseApp
 */

var app = angular.module('showcaseApp')
  .controller('RechercheProjetCtrl',['$http','GetProjects','DeleteProjects','EditProject', function($http, GetProjects,DeleteProjects,EditProject) {
    var self = this;
    this.projects=[];
    this.specificProject=false;
    this.editProject=false;
    this.init=function(){
      GetProjects.get('',
        function(data){
          self.projects=data.data;
        },
        function(data){
          console.log(data);
        });
      self.projet=[];
      self.users=[];
    }
    this.init();

    this.chercher="";
    this.chercherNom="";
    this.rechercherProjetParNom = function(){
      self.chercherNom=self.chercher;
    };

    this.clearSearch=function(){
      self.chercher="";
      self.chercherNom="";
    };

    this.supprimer = function(id){
      DeleteProjects.delete(id);
      self.init();
      self.retour();
    };

    this.afficherNom = function(id){
      GetProjects.get(id,
        function(data){
          self.specificProject = true;
          self.projet= data.data;
        },
        function(data){
          console.log(data);
        });
      GetProjects.getUsers(id,
        function(data){
          self.specificUser = true;
          self.users= data.data;
        },
        function(data){
          console.log(data);
        });

      this.editer=function(id){
        self.editProject=true;
      };
    };

    this.rolesList={};
    this.getRole=function(id,roles){
      angular.forEach(roles,function(value, index){
        if(value.ProjectId === id){
          self.rolesList[id]=value.name;
          console.log(value.name);
        }
      });
    };

    this.retour = function(){
      self.init();
      self.specificProject = false;
    };

    this.finEdition=function(){
      self.editProject=false;
    };

    this.editerValide=function(){
      EditProject.edit(self.projet.id,self.projet.title,self.projet.description,
        self.projet.year,function(){
          self.editProject=false;
        },
        function(){
          self.editProject=true;
          console.log('error edit');
        }
      )};
  }]);

app.service('GetProjects', ['$http', function GetProjects($http){
  this.get=function(userId, successFunc, failFunc){
    $http.get('http://poo-ihm-2015-rest.herokuapp.com/api/Projects/'+userId)
      .success(function(data) {
        if(data.status==="success"){
          successFunc(data);
        }else{
          failFunc(data);
        }
      });
  };

  this.getUsers=function(userId, successFunc, failFunc){
    $http.get('http://poo-ihm-2015-rest.herokuapp.com/api/Projects/'+userId+'/Users')
      .success(function(data) {
        if(data.status==="success"){
          successFunc(data);
        }else{
          failFunc(data);
        }
      });
  };
}]);

app.service('DeleteProjects',['$http',function DeleteProjects($http){
  this.delete=function(id){
    $http.delete('http://poo-ihm-2015-rest.herokuapp.com/api/Projects/'+id);
  };
}]);

app.directive("rechercherProjet",function(){
  return {
    restrict: "E",
    templateUrl:"views/rechercher-projet.html"
  }
});

app.directive("afficheProjet", function() {
  return {
    restrict: "E",
    templateUrl: "views/affiche-projet.html"
  };
});

app.directive("editerProjet", function() {
  return {
    restrict: "E",
    templateUrl: "views/editer-projet.html"
  };
});

app.service('EditProject',['$http', function EditProject($http){
  this.edit=function(id,title,description,year,successFunc,failFunc){
    this.newProject={};
    this.newProject.id=id;
    this.newProject.title= title;
    this.newProject.description= description;
    this.newProject.year= year;
    $http.put('http://poo-ihm-2015-rest.herokuapp.com/api/Projects/'+id, this.newProject)
      .success(function(data) {
        if(data.status==="success"){
          successFunc();
        }else{
          failFunc();
        }
      });
  }
}]);
