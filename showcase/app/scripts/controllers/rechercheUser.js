/**
 * @ngdoc function
 * @name showcaseApp.controller:RechercheCtrl
 * @description
 * # RechercheCtrl
 * Controller of the search in the showcaseApp
 */
var app = angular.module('showcaseApp')
  .controller('RechercheCtrl',['$http','GetUsers','DeleteUser','EditUser', function($http, GetUsers, DeleteUser, EditUser){

    var self = this;
    this.users=[];
    this.specificUser = false;
    this.editUser=false;

    this.init=function(){
      GetUsers.get('',
        function(data){
          self.users=data.data;
        },
        function(data){
          console.log(data);
        });
      self.roles=[];
      self.utilisateur=[];
      self.projects=[];
    }
    this.init();

    this.chercher="";
    this.chercherNom="";
    this.rechercherUtilisateurParNom = function(){
      self.chercherNom=self.chercher;
    };

    this.supprimer = function(id){
      DeleteUser.delete(id);
      self.init();
      self.retour();
    };

    this.afficherNom = function(id){
      GetUsers.getRole(id,
        function(data){
          self.specificUser = true;
          self.roles=data.data;
        },
        function(data){
          console.log(data);
        });
      GetUsers.get(id,
        function(data){
          self.specificUser = true;
          self.utilisateur= data.data;
        },
        function(data){
          console.log(data);
        });
      GetUsers.getProjects(id,
        function(data){
          self.specificUser = true;
          self.projects= data.data;
        },
        function(data){
          console.log(data);
        });
    };

    this.retour = function(){
      self.init();
      self.specificUser = false;
    };

    this.clearSearch=function(){
      self.chercher="";
      self.chercherNom="";
    };

    this.editer=function(id){
      self.editUser=true;
    };

    this.finEdition=function(){
      self.editUser=false;
    };

    this.editerValide=function(){
      EditUser.edit(self.utilisateur.id,self.utilisateur.name,self.utilisateur.surname,
        self.utilisateur.email,self.utilisateur.website,function(){
          self.editUser=false;
        },
        function(){
          self.editUser=true;
          console.log('error edit');
        }
      )};

  }]);

app.service('GetUsers', ['$http', function GetUsers($http){
  this.get=function(userId, successFunc, failFunc){
    $http.get('http://poo-ihm-2015-rest.herokuapp.com/api/Users/'+userId)
      .success(function(data) {
        if(data.status==="success"){
          successFunc(data);
        }else{
          failFunc(data);
        }
      });
  },
  this.getRole=function(userId, successFunc, failFunc){
    $http.get('http://poo-ihm-2015-rest.herokuapp.com/api/Users/'+userId+'/Roles')
      .success(function(data) {
        if(data.status==="success"){
          successFunc(data);
        }else{
          failFunc(data);
        }
      });
    };

  this.getProjects=function(userId, successFunc, failFunc){
    $http.get('http://poo-ihm-2015-rest.herokuapp.com/api/Users/'+userId+'/Projects')
      .success(function(data) {
        if(data.status==="success"){
          successFunc(data);
        }else{
          failFunc(data);
        }
      });
  };
}]);

app.service('DeleteUser',['$http',function DeleteUser($http){
  this.delete=function(id){
    $http.delete('http://poo-ihm-2015-rest.herokuapp.com/api/Users/'+id);
  }
}]);

app.service('EditUser',['$http', function EditUser($http){
  this.edit=function(id,name,surname,mail,site,successFunc,failFunc){
    this.newUser={};
    this.newUser.id=id;
    this.newUser.name= name;
    this.newUser.surname= surname;
    this.newUser.email= mail;
    this.newUser.website= site;
    this.newUser.createdAt=null;
    this.newUser.updatedAt=null;
    $http.put('http://poo-ihm-2015-rest.herokuapp.com/api/Users/'+id, this.newUser)
      .success(function(data) {
        if(data.status==="success"){
          successFunc();
        }else{
          failFunc();
        }
      });
  }
}]);

app.directive("afficheUtilisateur", function() {
  return {
    restrict: "E",
    templateUrl: "views/affiche-utilisateur.html",
    controller: "RechercheProjetCtrl",
    controllerAs: "searchProjectCtrl"
  };
});

app.directive("rechercherUtilisateur",function(){
  return {
    restrict: "E",
    templateUrl:"views/rechercher-utilisateur.html"
  }
});
app.directive("editerUtilisateur",function(){
  return{
    restrict: "E",
    templateUrl:"views/editer-utilisateur.html"
  }
});
